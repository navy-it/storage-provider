# Storage Provider

The default storage provider for Hazel. The author
is Wade Kallhoff. I have just packed it into a npm
package for consumption in my version of Hazel.

Extend this class to create another storage provider.
